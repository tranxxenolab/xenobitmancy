# Xenobitmancy, or how to have an encounter with quantum queerness

Jupyter notebook that explores the space of quantum noise for the purpose of divination. Produced in the context of [Aerosolnauts: Traveling Time, Aerosol Wormholes](https://biofriction.org/biofriction/travelling-time-aerosol-wormholes/).

# License

This work is licensed under the [Peer Production License](https://wiki.p2pfoundation.net/Peer_Production_License).
